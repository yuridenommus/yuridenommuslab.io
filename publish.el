(require 'ox-publish)
(defvar build-directory (concat default-directory "/build"))
(setq org-publish-use-timestamps-flag nil)
(setq org-publish-timestamp-directory (concat default-directory "/org-timestamps"))
(setq org-publish-project-alist
      `(("org-notes"
         :base-directory ,default-directory
         :base-extension "org"
         :publishing-directory ,build-directory
         :recursive t
         :publishing-function org-html-publish-to-html
         :auto-preamble t)
        ("org-static"
         :base-directory ,default-directory
         :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf"
         :publishing-directory ,build-directory
         :recursive t
         :publishing-function org-publish-attachment)
        ("org"
         :components ("org-notes" "org-static")
         :auto-sitemap t)))

(org-publish-project "org")
